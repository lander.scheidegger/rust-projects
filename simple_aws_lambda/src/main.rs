use lambda_runtime::{handler_fn, Context, Error};
use serde::{Deserialize, Serialize};

#[derive(Deserialize)]
struct CustomEvent {
    name: String,
    age: u8,
}

#[derive(Serialize)]
struct CustomOutput {
    req_id: String,
    msg: String,
}

#[tokio::main]
async fn main() -> Result<(), Error> {
    let func = handler_fn(my_handler);
    lambda_runtime::run(func).await?;
    Ok(())
}

pub(crate) async fn my_handler(event: CustomEvent, ctx: Context) -> Result<CustomOutput, Error> {
    // prepare the response
    let resp = CustomOutput {
        req_id: ctx.request_id,
        msg: format!("{} is {} years old!", event.name, event.age),
    };

    // return `Response` (it will be serialized to JSON automatically by the runtime)
    Ok(resp)
}
