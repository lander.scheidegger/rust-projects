extern crate yaml_rust;
use yaml_rust::YamlLoader;

use axum::{
    response::Redirect,
    routing::{get, MethodRouter},
    Router,
};

use std::collections::HashMap;

pub fn map_string_handler(url_map: HashMap<String, String>) -> Router {
    let mut app = Router::new();

    for (orig_route, redirect_url) in url_map.into_iter() {
        app = app.merge(route(
            &orig_route,
            get(|| async move { Redirect::to(&redirect_url) }),
        ));
    }

    app
}

pub fn map_yaml_handler(url_map: &str, mut app: Router) -> Router {
    let docs = YamlLoader::load_from_str(url_map).unwrap();

    if !docs.is_empty() {
        let doc = &docs[0];

        for url_item in doc.clone().into_vec().unwrap() {
            let test = url_item["url"].clone().into_string().unwrap();

            app = app.merge(route(
                url_item["path"].as_str().unwrap(),
                get(|| async move { Redirect::to(&test) }),
            ));
        }
    }

    app
}

fn route(path: &str, method_router: MethodRouter) -> Router {
    Router::new().route(path, method_router)
}
