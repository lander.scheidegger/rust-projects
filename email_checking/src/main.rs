use std::io::stdin;
use trust_dns_resolver::config::*;
use trust_dns_resolver::Resolver;

fn main() -> Result<(), std::io::Error> {
    println!("domain,hasMX,hasSPF,spfRecord,hasDMARC,dmarcRecord");
    println!("Please enter a domain to check, results will be in above format:");

    let mut domain_to_check = String::new();
    stdin().read_line(&mut domain_to_check)?;

    check_domain(domain_to_check.trim_end());

    Ok(())
}

fn check_domain(domain: &str) {
    let mut has_mx: bool = false;
    let mut has_spf: bool = false;
    let mut has_dmarc: bool = false;

    let mut spf_record: String = String::from("");
    let mut dmarc_record: String = String::from("");

    let resolver = Resolver::new(ResolverConfig::default(), ResolverOpts::default()).unwrap();

    if let Ok(mx_response) = resolver.mx_lookup(domain) {
        if mx_response.iter().peekable().peek().is_some() {
            has_mx = true;
        }
    } else {
        println!("No 'mx' records found");
    }

    if let Ok(txt_response) = resolver.txt_lookup(domain) {
        for record in txt_response {
            if record.to_string().starts_with("v=spf1") {
                has_spf = true;
                spf_record = record.to_string();
                break;
            }
        }
    } else {
        println!("No 'txt' records found");
    }

    if let Ok(dmarc_records) = resolver.txt_lookup(String::from("_dmarc.") + domain) {
        for record in dmarc_records {
            if record.to_string().starts_with("v=DMARC1") {
                has_dmarc = true;
                dmarc_record = record.to_string();
                break;
            }
        }
    } else {
        println!("No 'dmarc' records found");
    }

    println!(
        "domain: {}, has_mx: {}, has_spf: {}, spfRecord: {}, has_dmarc: {}, DMARC Record: {}",
        domain, has_mx, has_spf, spf_record, has_dmarc, dmarc_record
    );
}
