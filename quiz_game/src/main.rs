extern crate csv;

use csv::ReaderBuilder;
use std::env;
use std::error::Error;
use std::io;
use tokio::sync::oneshot;
use tokio::time::{interval_at, Duration, Instant};

#[tokio::main]
async fn main() -> Result<(), Box<dyn std::error::Error>> {
    // ** this section for timed quiz **
    let args: Vec<String> = env::args().collect();
    let game_time_seconds = if args.len() > 1 {
        args[1].parse().unwrap()
    } else {
        30
    };
    let _ = check_answers_for_csv_timed(game_time_seconds).await;
    // -- end of timed quiz section

    // ** this code is for untimed quiz
    // let _ = check_answers_for_csv();

    Ok(())
}

fn check_answers_for_csv() -> Result<(), Box<dyn Error>> {
    let mut csv_reader = ReaderBuilder::new()
        .has_headers(false)
        .from_path("problems.csv")?;

    let mut question_count = 0;
    let mut correct_answer_count = 0;

    for quiz_row in csv_reader.records() {
        let record = quiz_row?;
        println!("Question: {}", record.get(0).unwrap());

        let answer = record.get(1).unwrap();

        let mut guess = String::new();
        io::stdin()
            .read_line(&mut guess)
            .expect("failed to read user guess");

        if guess.trim() == answer {
            correct_answer_count += 1;
        }
        question_count += 1;

        if question_count >= 5 {
            break;
        }
    }

    println!(
        "Total questions: {} and correct answers: {}",
        question_count, correct_answer_count,
    );

    Ok(())
}

async fn check_answers_for_csv_timed(game_time_in_seconds: u64) -> Result<(), Box<dyn Error>> {
    let mut csv_reader = ReaderBuilder::new()
        .has_headers(false)
        .from_path("problems.csv")?;

    let mut question_count = 0;
    let mut correct_answer_count = 0;

    let mut user_wait = String::new();
    println!("Please press enter when ready for quiz...");
    let _ = io::stdin().read_line(&mut user_wait);

    let mut wait_interval = interval_at(
        Instant::now() + Duration::from_secs(game_time_in_seconds),
        Duration::from_millis(500),
    );

    'loop_end_label: for quiz_row in csv_reader.records() {
        let record = quiz_row?;
        println!("Question: {}", record.get(0).unwrap());
        question_count += 1;

        let answer = record.get(1).unwrap();

        let (send, mut recv) = oneshot::channel();
        let input_task = tokio::spawn(async move {
            let mut guess = String::new();
            io::stdin()
                .read_line(&mut guess)
                .expect("failed to read user guess");

            let _ = send.send(guess);
        });

        loop {
            tokio::select! {
                           _ = wait_interval.tick() => {
                                    println!("\n\nProgram ended.");
                                    input_task.abort();
                                    break 'loop_end_label
                            },
                           msg = &mut recv => {
                                 if msg.unwrap().trim() == answer {
                                    correct_answer_count += 1;
                                }
                               break;
                           }
            }
        }
    }

    println!(
        "\n*****Total questions: {} and correct answers: {}",
        question_count, correct_answer_count,
    );

    Ok(())
}
