use std::env;
use std::error::Error;

fn main() -> Result<(), Box<dyn Error>> {
    let args: Vec<String> = env::args().collect();

    // camel_case_count(&args);
    caesar_cipher_encrypt(&args)?;

    Ok(())
}

fn camel_case_count(args: &Vec<String>) {
    println!("** printing camel case word count **");
    for user_value in args[1..].iter() {
        let mut word_count = 1;

        for character in user_value.chars() {
            if character.is_ascii_uppercase() {
                word_count += 1;
            }
        }
        println!(
            "Input value is: {} and word count is : {}",
            user_value, word_count
        );
    }
}

fn caesar_cipher_encrypt(args: &Vec<String>) -> Result<(), Box<dyn Error>> {
    println!("** printing encrypted ceaser cipher text **");
    println!("original text: {}", &args[2]);
    println!("rotation offset: {}", &args[3]);

    let original_text = args[2].as_str();
    let offset = args[3].parse::<u8>()?;

    // for optimization, create string with input string size
    let mut encrypted_text = String::with_capacity(args[1].parse::<usize>()?);

    for current_char in original_text.chars() {
        // if user text is not ascii alphabet then skip encryption
        if !current_char.is_ascii_alphabetic() {
            encrypted_text.push(current_char);
            continue;
        }

        // we need to find the base ascii value so we can normalize values between 0 and 25
        let base = if current_char.is_ascii_uppercase() {
            b'A'
        } else {
            b'a'
        };

        // convert alphabet to ascii value
        let char_byte = current_char as u8;

        // here we are normalizing value so A-Z or a-z will be between 0-25
        let normalize_byte = char_byte - base;

        // rotate value by adding cypher offset , then "mod" by 26 so numbers at end of range will rotate back to >=0
        // then add "base" back to get the actual alphabet ascii value
        let rotate_text = ((normalize_byte + offset) % 26) + base;

        encrypted_text.push(rotate_text as char);
    }

    println!("encrypted resulted text: {}", encrypted_text);

    Ok(())
}
